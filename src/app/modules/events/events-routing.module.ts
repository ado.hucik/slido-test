import { NgModule } from '@angular/core';
import {EventsComponent} from './events.component';
import {RouterModule, Routes} from '@angular/router';
import {EditEventComponent} from './edit-event/edit-event.component';

const routes: Routes = [
  {
    path: '',
    component: EventsComponent
  },
  {
    path: 'edit/:id',
    component: EditEventComponent
  },
  {
    path: 'new',
    component: EditEventComponent
  }

];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class EventsRoutingModule { }

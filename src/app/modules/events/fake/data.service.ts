import {Injectable} from '@angular/core';
import {InMemoryDbService} from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})
export class DataService implements InMemoryDbService{

  constructor() { }
  createDb(){
    const  events =  [
      {
        id: 1,
        title: 'Concert',
        status: 'Active',
        eventDate: '2021-02-19T21:50',
        description: 'This is concert',
        city: '',
        zip: '',
        country: '',
        houseNumber: '',
        invitedPeople: [],
        isPublic: true,
        eventCreatedBy: '',
        lat: 10,
        lon: 10
      },
      {
        id: 2,
        title: 'event',
        status: 'Inactive',
        eventDate: '2021-02-10T21:50',
        description: 'This is concert',
        city: '',
        zip: '',
        country: '',
        houseNumber: '',
        invitedPeople: [],
        isPublic: true,
        eventCreatedBy: '',
        lat: 10,
        lon: 10
      }
    ];

    return {events};

  }
}

import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {EventModel} from '../models/event.model';
import {ActivatedRoute} from '@angular/router';
import {EventService} from '../services/event.service';
import {Subscription} from 'rxjs';
import {WeatherService} from '../../../shared/services/weather.service';
import * as moment from 'moment';

@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.css']
})
export class EditEventComponent implements OnInit, OnDestroy {
  eventForm: FormGroup;
  event: EventModel;
  eventId: number;
  subscriptions: Subscription[];
  weatherData: any;
  isWeather = false;

  constructor(
    private  fb: FormBuilder,
    private route: ActivatedRoute,
    private eventService: EventService,
    private weatherService: WeatherService
  ) {
    this.subscriptions = [];
    this.route.params.subscribe(params => {
      this.eventId = +params.id;
      if (this.eventId) {
        this.getEvent(this.eventId);
      } else {
        this.createForm();
      }
    });
  }

  ngOnInit(): void {}

  getWeatherData(lat, lon): void {
    this.weatherService.getWeather(lat, lon)
      .then(response => response.json())
      .then(data => {
        this.setWeatherData(data);
      });
  }

  setWeatherData(data): void {
    const eventDate = moment(this.eventForm.get('eventDate').value);
    const today = moment();
    const diff = eventDate.diff(today, 'days');
    if (data.daily[diff]) {
      this.weatherData = data.daily[diff];
      const sunsetTime = new Date(this.weatherData.sunset * 1000);
      this.weatherData.sunset_time = sunsetTime.toLocaleTimeString();
      const currentDate = new Date();
      this.weatherData.isDay = (currentDate.getTime() < sunsetTime.getTime());
      this.weatherData.temp_celcius = (this.weatherData.temp.day - 273.15).toFixed(0);
      this.weatherData.temp_min = (this.weatherData.temp.min - 273.15).toFixed(0);
      this.weatherData.temp_max = (this.weatherData.temp.max - 273.15).toFixed(0);
      this.weatherData.temp_feels_like = (this.weatherData.feels_like - 273.15).toFixed(0);
      this.isWeather = true;
    } else {
      this.isWeather = false;
    }
  }

  getEvent(eventId: number): void {
    this.subscriptions.push(this.eventService.getEventById(eventId).subscribe((event: EventModel) => {
      this.loadForm(event);
      this.event = event;
      this.getWeatherData(event.lat, event.lon);
    }));
  }

  createForm(): void {
    this.eventForm = this.fb.group({
      id: [Math.floor(Math.random() * 10000)],
      title: [''],
      status: ['Active'],
      eventDate: [''],
      description: [''],
      city: [''],
      zip: [''],
      country: [''],
      houseNumber: [''],
      invitedPeople: [],
      isPublic: true,
      eventCreatedBy: [''],
      lat: [''],
      lon: ['']
    });
  }

  loadForm(event: EventModel): void {
    this.eventForm = this.fb.group({
      id: [event.id],
      title: [event.title],
      status: [event.status],
      eventDate: [event.eventDate],
      description: [event.description],
      city: [event.city],
      zip: [event.zip],
      country: [event.country],
      houseNumber: [event.houseNumber],
      invitedPeople: [event.invitedPeople],
      isPublic: [event.isPublic],
      eventCreatedBy: [event.eventCreatedBy],
      lat: [event.lat],
      lon: [event.lon]
    });
  }

  save(): void {
    if (this.eventForm.get('id')) {
      this.eventService.updateEvent(this.eventForm.value);
    } else {
      this.eventService.createEvent(this.eventForm.value);
    }
  }

  getWeather(): void {
    if (this.eventForm.get('lat').value && this.eventForm.get('lon').value) {
      this.getWeatherData(this.eventForm.get('lat').value, this.eventForm.get('lon').value);
    }
  }

  ngOnDestroy(): void {
    if (this.subscriptions) {
      this.subscriptions.forEach((subscription: Subscription) => {
        subscription.unsubscribe();
      });
    }
  }
}

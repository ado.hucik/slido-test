import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {EventService} from './services/event.service';
import {EventModel} from './models/event.model';
import {Router} from '@angular/router';
import {MatTableDataSource} from '@angular/material/table';
import {FormControl} from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventsComponent implements OnInit {
  displayedColumns: string[] = ['id', 'title', 'description', 'status', 'buttons'];
  dataSource: MatTableDataSource<EventModel>;
  eventStatus = new FormControl('All');
  events: EventModel[];

  constructor(
    public eventService: EventService,
    public cdr: ChangeDetectorRef,
    public router: Router
  ) {
  }

  ngOnInit(): void {
    this.getEvents();
  }

  getEvents(): void {
    this.eventService.getEvents();
    this.eventService.eventObservable.subscribe((events: EventModel[]) => {
      if (events) {
        this.dataSource = new MatTableDataSource<EventModel>(events);
        this.events = events;
        this.cdr.detectChanges();
      }
    });
  }

  pastOrUpcoming(): void {
    if (this.eventStatus.value === 'Past') {
      const filteredData = this.events.filter((event: EventModel) => moment().format('YYYY-MM-DDTHH:mm') > event.eventDate);
      this.dataSource = new MatTableDataSource<EventModel>(filteredData);
    } else if (this.eventStatus.value === 'Upcoming') {
      const filteredData = this.events.filter((event: EventModel) => moment().format('YYYY-MM-DDTHH:mm') < event.eventDate);
      this.dataSource = new MatTableDataSource<EventModel>(filteredData);
    } else {
      this.dataSource = new MatTableDataSource<EventModel>(this.events);
    }
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  navigate(eventId): void {
    this.router.navigateByUrl('edit/' + eventId);
  }

  newEvent(): void {
    this.router.navigateByUrl('new');
  }
}

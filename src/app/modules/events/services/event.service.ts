import {Injectable, OnDestroy} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {BehaviorSubject, Observable} from 'rxjs';
import {EventModel} from '../models/event.model';
import {Router} from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class EventService {
  eventsSource: BehaviorSubject<EventModel[]> = new BehaviorSubject<EventModel[]>(null);
  eventObservable = this.eventsSource.asObservable();

  constructor(
    private httpClient: HttpClient,
    private router: Router
  ) {

  }

  public getEvents(): void {
    this.httpClient.get(environment.apiUrl + 'api/events').subscribe((events: EventModel[]) => {
      this.eventsSource.next(events);
    });
  }

  public getEventById(eventId: number): Observable<EventModel> {
    return this.httpClient.get(environment.apiUrl + `api/events/${eventId}`);
  }

  public updateEvent(event: EventModel): void {
    this.httpClient.put(environment.apiUrl + `api/events/${event.id}`, event).subscribe(
      response => {
        this.getEvents();
        this.router.navigate(['../']);
      }
    );
  }

  public createEvent(event: EventModel): void {
    this.httpClient.post(environment.apiUrl + `api/events/${event.id}`, event).subscribe(
      response => {
        this.getEvents();
        this.router.navigate(['../']);
      }
    );
  }
}

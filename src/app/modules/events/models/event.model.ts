export class EventModel {
  constructor(
    public id?: number,
    public title?: string,
    public status?: string,
    public eventDate?: string,
    public description?: string,
    public city?: string,
    public zip?: string,
    public country?: string,
    public houseNumber?: string,
    public invitedPeople?: [],
    public isPublic?: string,
    public eventCreatedBy?: string,
    public lat?: number,
    public lon?: number
  ) {
  }
}

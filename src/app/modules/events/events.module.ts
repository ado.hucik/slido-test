import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EventsComponent} from './events.component';
import {EventsRoutingModule} from './events-routing.module';
import {MaterialModule} from '../../shared/modules/material/material.module';
import { EditEventComponent } from './edit-event/edit-event.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [EventsComponent, EditEventComponent],
  imports: [
    CommonModule,
    EventsRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class EventsModule { }

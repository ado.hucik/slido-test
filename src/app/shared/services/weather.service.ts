import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor() {
  }


  getWeather(lat, lon): Promise<any> {
    return fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&appid=e79d60cc72f9b5a2fd928dc543be81a7`);
  }
}

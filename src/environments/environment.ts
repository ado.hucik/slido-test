export const environment = {
  production: false,
  isMockEnabled: true,
  appVersion: 'v717demo2',
  USERDATA_KEY: 'authf649fc9a5f55',
  apiUrl: 'http://localhost:4200/'
};
